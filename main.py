"""
Snake -- by Aquaxoc
"""

import pygame
from random import randint

pygame.init()
window = pygame.display.set_mode((600, 600))
score = 0
pygame.display.set_caption("Snake | Score: "+str(score))
clock = pygame.time.Clock()
restartGame = True

def redrawGame(snake, apple):
    window.fill((0,0,0))
    for e in snake:
        pygame.draw.rect(window, (0, 255, 0), (e[0]+1, e[1]+1, 18, 18))
    pygame.draw.rect(window, (255, 0, 0), (apple[0]+1, apple[1]+1, 18, 18))
    pygame.display.flip()


while restartGame:
    snake = [(40, 0), (20, 0), (0, 0)]
    score = 0
    pygame.display.set_caption("Snake | Score: "+str(score))
    direction = "right"
    nextdirection = "right"
    gameloop = True
    gameover = False
    pause = False

    def addcoords(tupple1, tupple2):
        return tupple1[0]+tupple2[0], tupple1[1]+tupple2[1]

    # Run the move code every 200 ms
    MOVEEVENT = pygame.USEREVENT+1
    pygame.time.set_timer(MOVEEVENT, 200)

    # First apple
    while True:
        apple = (randint(0, 29)*20, randint(0, 29)*20)
        if apple not in snake:
            pygame.draw.rect(window, (255, 0, 0), (apple[0]+1, apple[1]+1, 18, 18))
            break

    # Initial print
    redrawGame(snake, apple)

    # Main loop
    while gameloop:
        for event in pygame.event.get():
            if pause:
                pygame.font.init()
                font = pygame.font.SysFont("Arial", 70)
                pausetext = font.render("Pause", False, (255, 255, 255))
                font = pygame.font.SysFont("Arial", 40)
                scoretext = font.render("Score: "+str(score), False, (255, 255, 255))
                window.blit(pausetext, (int(300-(pausetext.get_width())/2), int(300-(pausetext.get_height()+scoretext.get_height())/2)))
                window.blit(scoretext, (int(300-scoretext.get_width()/2), int(300+scoretext.get_height()/2)))
                # Buttons
                continueTxt = font.render("Continue", False, (255, 255, 255))
                pygame.draw.rect(window, (0, 100, 0), (int(300-continueTxt.get_width()/2)-5, int(450-continueTxt.get_height()/2)-2, continueTxt.get_width()+10, continueTxt.get_height()+4))
                continueButton = window.blit(continueTxt, (int(300-continueTxt.get_width()/2), int(450-continueTxt.get_height()/2)))
                quitTxt = font.render("Quit", False, (255, 255, 255))
                quitButton = pygame.draw.rect(window, (100, 0, 0), (int(300-quitTxt.get_width()/2)-5, int(500-quitTxt.get_height()/2)-2, quitTxt.get_width()+10, quitTxt.get_height()+4))
                window.blit(quitTxt, (int(300-quitTxt.get_width()/2), int(500-quitTxt.get_height()/2)))
                # Update the display
                pygame.display.flip()
                while pause:
                    for ev in pygame.event.get():
                        if (ev.type == pygame.MOUSEBUTTONDOWN and ev.button == pygame.BUTTON_LEFT):
                            #restartTxt = restartTxt.get_rect()
                            #squitTxt = quitTxt.get_rect()
                            pos = pygame.mouse.get_pos()
                            if continueButton.collidepoint(pos):
                                pause = False
                                break
                            elif quitButton.collidepoint(pos):
                                pause = False
                                gameover = False
                                gameloop = False
                                restartGame = False
                                break
                        elif ev.type == pygame.KEYDOWN and ev.key == pygame.K_ESCAPE:
                            pause = False
                            break
                        elif ev.type == pygame.QUIT:
                            pause = False
                            gameover = False
                            gameloop = False
                            restartGame = False
                            break
                redrawGame(snake, apple)
            if event.type == pygame.QUIT:
                gameloop = False
                gameover = True
                break
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                pause = not pause
                redrawGame(snake, apple)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    nextdirection = "up"
                elif event.key == pygame.K_DOWN:
                    nextdirection = "down"
                elif event.key == pygame.K_LEFT:
                    nextdirection = "left"
                elif event.key == pygame.K_RIGHT:
                    nextdirection = "right"
            elif event.type == MOVEEVENT and not pause:
                if nextdirection == "up" and (direction == "left" or direction == "right"):
                    direction = "up"
                elif nextdirection == "down" and (direction == "left" or direction == "right"):
                    direction = "down"
                elif nextdirection == "left" and (direction == "up" or direction == "down"):
                    direction = "left"
                elif nextdirection == "right" and (direction == "up" or direction == "down"):
                    direction = "right"
                lastsnakepart = snake[-1]
                if direction == "right":
                    pygame.draw.rect(window, (0, 0, 0), (snake[-1][0]+1, snake[-1][1]+1, 18, 18))
                    del snake[-1]
                    snake.insert(0, addcoords(snake[0], (20, 0)))
                elif direction == "left":
                    pygame.draw.rect(window, (0, 0, 0), (snake[-1][0]+1, snake[-1][1]+1, 18, 18))
                    del snake[-1]
                    snake.insert(0, addcoords(snake[0], (-20, 0)))
                elif direction == "up":
                    pygame.draw.rect(window, (0, 0, 0), (snake[-1][0]+1, snake[-1][1]+1, 18, 18))
                    del snake[-1]
                    snake.insert(0, addcoords(snake[0], (0, -20)))
                elif direction == "down":
                    pygame.draw.rect(window, (0, 0, 0), (snake[-1][0]+1, snake[-1][1]+1, 18, 18))
                    del snake[-1]
                    snake.insert(0, addcoords(snake[0], (0, 20)))
                pygame.draw.rect(window, (0, 255, 0), (snake[0][0]+1, snake[0][1]+1, 18, 18))
                # Apple
                if snake[0] == apple:
                    # Snake grows by one
                    snake.append(lastsnakepart)
                    score += 1
                    pygame.display.set_caption("Snake | Score: "+str(score))
                    # Generate a new apple
                    while True:
                        apple = (randint(0, 29) * 20, randint(0, 29) * 20)
                        if apple not in snake:
                            pygame.draw.rect(window, (255, 0, 0), (apple[0] + 1, apple[1] + 1, 18, 18))
                            break

        # Fail conditions
        if (not 0 <= snake[0][0] <= 590 or not 0 <= snake[0][1] <= 590) or (snake[0] in snake[1:]):
            gameloop = False
            gameover = True

        # Update the display
        pygame.display.flip()

    # Gameover display
    if gameover:
        pygame.font.init()
        font = pygame.font.SysFont("Arial", 70)
        gameovertext = font.render("Game over", False, (255, 255, 255))
        font = pygame.font.SysFont("Arial", 40)
        scoretext = font.render("Score: "+str(score), False, (255, 255, 255))
        window.blit(gameovertext, (int(300-(gameovertext.get_width())/2), int(300-(gameovertext.get_height()+scoretext.get_height())/2)))
        window.blit(scoretext, (int(300-scoretext.get_width()/2), int(300+scoretext.get_height()/2)))
        # Buttons
        restartTxt = font.render("Restart", False, (255, 255, 255))
        pygame.draw.rect(window, (0, 100, 0), (int(300-restartTxt.get_width()/2)-5, int(450-restartTxt.get_height()/2)-2, restartTxt.get_width()+10, restartTxt.get_height()+4))
        restartButton = window.blit(restartTxt, (int(300-restartTxt.get_width()/2), int(450-restartTxt.get_height()/2)))
        quitTxt = font.render("Quit", False, (255, 255, 255))
        quitButton = pygame.draw.rect(window, (100, 0, 0), (int(300-quitTxt.get_width()/2)-5, int(500-quitTxt.get_height()/2)-2, quitTxt.get_width()+10, quitTxt.get_height()+4))
        window.blit(quitTxt, (int(300-quitTxt.get_width()/2), int(500-quitTxt.get_height()/2)))
        # Update the display
        pygame.display.flip()
    while gameover:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                gameover = False
                restartGame = False
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == pygame.BUTTON_LEFT:
                #restartTxt = restartTxt.get_rect()
                #squitTxt = quitTxt.get_rect()
                pos = pygame.mouse.get_pos()
                if restartButton.collidepoint(pos):
                    gameover = False
                    gameloop = True
                elif quitButton.collidepoint(pos):
                    gameover = False
                    restartGame = False

pygame.quit()
